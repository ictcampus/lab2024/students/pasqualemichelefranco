package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @author Pipposultavolino
 * @since 1.0.0
 */
@Value
@Builder
@Jacksonized
public class WorldRequest {
	private Long id;
	private String name;
	private String system;


}
