package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.service.world.WordService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
///home/admin/Scaricati/base-simple-main/src/main/java/com/ictcampus/lab/base/control/book/model/base/control/WorldController.java
///home/admin/Scaricati/base-simple-main/src/main/java/com/ictcampus/lab/base/control/WordController.java
@RestController
@RequestMapping("/api/v1/words")
@AllArgsConstructor
@Slf4j
public class WordController {

	private final WordService wordService;

	@GetMapping(value = "/init", produces = MediaType.APPLICATION_JSON_VALUE)
	public String init() {
		wordService.init();
		return "Init OK";
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<WorldResponse> getWords() {
		log.info("Returning the list of words");
		return wordService.getWorlds();
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public WorldResponse getWord(@PathVariable(name = "id") Long id) {
		log.info("Returning word with ID: {}", id);
		return wordService.getWorld(id);
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Long createWorld(@RequestBody WorldRequest worldRequest) {
		log.info("Creating new word: {}", worldRequest);
		return wordService.createWorld(worldRequest);
	}

	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void editWorld(@PathVariable(name = "id") Long id, @RequestBody WorldRequest worldRequest) {
		log.info("Updating word with ID [{}] and data [{}]", id, worldRequest);
		wordService.editWorld(id, worldRequest);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void deleteWord(@PathVariable(name = "id") Long id) {
		log.info("Deleting word with ID: {}", id);
		wordService.deleteWorld(id);
	}
}
