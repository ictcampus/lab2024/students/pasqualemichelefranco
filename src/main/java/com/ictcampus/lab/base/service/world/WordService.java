package com.ictcampus.lab.base.service.world;

import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.control.exception.*;
import com.ictcampus.lab.base.repository.world.WorldRepository;
import com.ictcampus.lab.base.control.repository.entity.WorldEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class WordService {

	@Autowired
	private final WorldRepository worldRepository;

	public List<WorldResponse> getWorlds() {
		log.info("Returning the list of worlds");
		List<WorldEntity> worldEntities = worldRepository.findAll();
		return worldEntities.stream()
				.map(this::convertToWorldResponse)
				.collect(Collectors.toList());
	}

	public WorldResponse getWorld(Long id) {
		log.info("Returning world with ID: {}", id);
		return Optional.ofNullable(worldRepository.findById(id))
				.map(this::convertToWorldResponse)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "World not found"));
	}

	public Long createWorld(WorldRequest worldRequest) {
		String newName = worldRequest.getName();
		if (doesWorldExistWithName(newName)) {
			log.warn("World with name '{}' already exists.", newName);
			throw new NameAlreadyExistsException();
		}

		long newId = worldRepository.create(newName, worldRequest.getSystem());
		log.info("Created a new world with ID '{}', name '{}', system '{}'.", newId, newName, worldRequest.getSystem());

		return newId;
	}

	public void editWorld(Long id, WorldRequest worldRequest) {
		log.info("Updating world with ID [{}] and data [{}]", id, worldRequest);
		int rowsAffected = worldRepository.update(id, worldRequest.getName(), worldRequest.getSystem());
		if (rowsAffected == 0) {
			log.warn("World with ID [{}] not found", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "World not found");
		}
	}

	public void deleteWorld(Long id) {
		log.info("Deleting world with ID: {}", id);
		int rowsAffected = worldRepository.delete(id);
		if (rowsAffected == 0) {
			log.warn("World with ID [{}] not found or already deleted", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "World not found");
		}
	}

	public void init() {
		log.info("Initializing world list");
		worldRepository.deleteAll(); // Assicurati di avere un metodo per cancellare tutti i record
		generateWorlds().forEach(world -> worldRepository.create(world.getName(), world.getSystem()));
	}

	private boolean doesWorldExistWithName(String name) {
		try {
			worldRepository.findByName(name);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private WorldResponse convertToWorldResponse(WorldEntity worldEntity) {
		return WorldResponse.builder()
				.id(worldEntity.getId())
				.name(worldEntity.getName())
				.system(worldEntity.getSystem())
				.build();
	}

	private List<WorldResponse> generateWorlds() {
		String[] names = {"Earth", "Mars", "Jupiter", "Venus", "Saturn"};
		return List.of(names).stream()
				.map(name -> WorldResponse.builder()
						.name(name)
						.system("Solar System")
						.build())
				.collect(Collectors.toList());
	}
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public class NameAlreadyExistsException extends RuntimeException {
		public NameAlreadyExistsException() {
			super("Name already exists");
		}
	}
}
