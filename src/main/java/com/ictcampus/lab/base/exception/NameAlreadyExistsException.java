package com.ictcampus.lab.base.control.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NameAlreadyExistsException extends RuntimeException {
    public NameAlreadyExistsException() {
        super("Name already exists");
    }
}
