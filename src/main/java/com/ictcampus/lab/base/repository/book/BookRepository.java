package com.ictcampus.lab.base.repository.book;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long>, BookRepositoryCustom {
	BookEntity findByTitle(String title);
}
