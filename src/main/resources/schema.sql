CREATE SCHEMA IF NOT EXISTS baseproject;
SET SCHEMA baseproject;

CREATE TABLE word(
                     id bigint auto_increment,
                     name VARCHAR(50) NOT NULL,
                     system VARCHAR(255)
);
-- LIBRARY / BOOKSHOP
CREATE TABLE books (
    id BIGINT AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    isbn VARCHAR(255) NOT NULL,
    abstract VARCHAR(255) NOT NULL,
    description CHARACTER LARGE OBJECT NULL,
    publisher VARCHAR(255) NOT NULL,
    published_date DATE NOT NULL,
    price FLOAT NULL,
    discount FLOAT NULL,
    position_id BIGINT NULL,
    thumbnail_id BIGINT NULL
);

CREATE UNIQUE INDEX IDX_BOOKS_ISBN ON books(isbn);
CREATE INDEX IDX_BOOKS_TITLE ON books(title);
CREATE INDEX IDX_BOOKS_PUBLISHER ON books(publisher);

CREATE TABLE positions (
    id BIGINT AUTO_INCREMENT,
    floor VARCHAR(25) NOT NULL,
    sector VARCHAR(25) NOT NULL,
    rack VARCHAR(25) NOT NULL,
    line VARCHAR(25) NOT NULL
);
CREATE UNIQUE INDEX IDX_POSITIONS_POS ON positions(floor, sector, rack, line);

CREATE TABLE authors (
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(255) NULL,
    surname VARCHAR(255) NOT NULL,
    nickname VARCHAR(255) NULL,
    birthday DATE NULL
);

CREATE UNIQUE INDEX IDX_AUTHORS_SURNAME ON authors(surname);
CREATE INDEX IDX_AUTHORS_NAME ON authors(name, surname, nickname);

CREATE TABLE images (
    id BIGINT AUTO_INCREMENT,
    title VARCHAR(255) NULL,
    url VARCHAR(1024) NOT NULL,
    is_thumbnail BOOLEAN NOT NULL DEFAULT false
);

CREATE UNIQUE INDEX IDX_IMAGES_URL ON images(url);
CREATE INDEX IDX_IMAGES_TITLE ON images(title);

CREATE TABLE book_author (
    book_id BIGINT NOT NULL,
    author_id BIGINT NOT NULL
);
CREATE UNIQUE INDEX IDX_BOOK_AUTHOR_PK ON book_author(book_id, author_id);

CREATE TABLE book_image (
    book_id BIGINT NOT NULL,
    image_id BIGINT NOT NULL
);
CREATE UNIQUE INDEX IDX_BOOK_IMAGE_PK ON book_image(book_id, image_id);

CREATE TABLE users (
    id BIGINT AUTO_INCREMENT,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(150) NOT NULL
);
CREATE UNIQUE INDEX IDX_USERS_USERNAME ON users(username);