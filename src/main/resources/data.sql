
INSERT INTO word (name ,system) values ('Earth','Sistema Solare');
INSERT INTO word (name ,system) values ('Mars','Sistema Solare');
INSERT INTO word (name ,system) values ('Jupiter','Sistema Solare');
INSERT INTO word (name ,system) values ('Venus','Sistema Solare');
INSERT INTO word (name ,system) values ('Saturn','Sistema Solare');

INSERT INTO authors(name, surname, nickname) VALUES ('rty', 'rty', 'rty');
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('thyrt', '23444555963-2', 'Lpippo', 'giovanni', '1301-01-01', 99.00, 0);
INSERT INTO book_author(book_id, author_id) VALUES (1, 1);

INSERT INTO users(username, password) VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6' );